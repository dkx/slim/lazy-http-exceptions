<?php

declare(strict_types=1);

namespace DKX\SlimLazyHttpExceptions;

use Psr\Http\Message\ServerRequestInterface;
use Slim;

final class HttpNotFoundException extends LazyHttpException
{
	public function toRealHttpException(ServerRequestInterface $request): Slim\Exception\HttpException
	{
		return new Slim\Exception\HttpNotFoundException($request, $this->getMessage(), $this);
	}
}
