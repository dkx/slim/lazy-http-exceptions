<?php

declare(strict_types=1);

namespace DKX\SlimLazyHttpExceptions;

use Psr\Http\Message\ServerRequestInterface;
use Slim;

final class HttpForbiddenException extends LazyHttpException
{
	public function toRealHttpException(ServerRequestInterface $request): Slim\Exception\HttpException
	{
		return new Slim\Exception\HttpForbiddenException($request, $this->getMessage(), $this);
	}
}
