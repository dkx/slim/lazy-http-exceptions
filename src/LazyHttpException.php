<?php

declare(strict_types=1);

namespace DKX\SlimLazyHttpExceptions;

use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpException;

abstract class LazyHttpException extends \RuntimeException
{
	abstract public function toRealHttpException(ServerRequestInterface $request): HttpException;
}
