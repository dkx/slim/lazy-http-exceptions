# DKX/Slim/LazyHttpExceptions

Request-less HTTP exceptions for Slim

## Installation

```bash
$ composer require dkx/slim-lazy-http-exceptions
```
