<?php

declare(strict_types=1);

namespace DKX\SlimLazyHttpExceptionsTests\Tests;

use DKX\SlimLazyHttpExceptions\HttpNotFoundException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim;

final class HttpNotFoundExceptionTest extends TestCase
{
	public function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testToRealHttpException(): void
	{
		$req = \Mockery::mock(ServerRequestInterface::class);

		$e = new HttpNotFoundException('not found');
		$ee = $e->toRealHttpException($req);

		self::assertInstanceOf(Slim\Exception\HttpNotFoundException::class, $ee);
		self::assertSame('not found', $ee->getMessage());
		self::assertSame($e, $ee->getPrevious());
	}
}
