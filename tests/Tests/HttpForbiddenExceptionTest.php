<?php

declare(strict_types=1);

namespace DKX\SlimLazyHttpExceptionsTests\Tests;

use DKX\SlimLazyHttpExceptions\HttpForbiddenException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim;

final class HttpForbiddenExceptionTest extends TestCase
{
	public function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testToRealHttpException(): void
	{
		$req = \Mockery::mock(ServerRequestInterface::class);

		$e = new HttpForbiddenException('forbidden');
		$ee = $e->toRealHttpException($req);

		self::assertInstanceOf(Slim\Exception\HttpForbiddenException::class, $ee);
		self::assertSame('forbidden', $ee->getMessage());
		self::assertSame($e, $ee->getPrevious());
	}
}
