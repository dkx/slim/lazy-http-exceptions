<?php

declare(strict_types=1);

namespace DKX\SlimLazyHttpExceptionsTests\Tests;

use DKX\SlimLazyHttpExceptions\HttpBadRequestException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim;

final class HttpBadRequestExceptionTest extends TestCase
{
	public function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testToRealHttpException(): void
	{
		$req = \Mockery::mock(ServerRequestInterface::class);

		$e = new HttpBadRequestException('bad request');
		$ee = $e->toRealHttpException($req);

		self::assertInstanceOf(Slim\Exception\HttpBadRequestException::class, $ee);
		self::assertSame('bad request', $ee->getMessage());
		self::assertSame($e, $ee->getPrevious());
	}
}
